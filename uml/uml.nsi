; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply 
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there. 

;--------------------------------

; The name of the installer
Name "uml"

; The file to write
OutFile "uml-setup.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\Unity Multiversion Launcher"

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\UnityMultiversionLauncher "Install_Dir" "$INSTDIR"

  File /r "files\*.*"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UnityMultiversionLauncher" "DisplayName" "Unity Multiversion Launcher"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UnityMultiversionLauncher" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UnityMultiversionLauncher" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UnityMultiversionLauncher" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

  CreateDirectory "$SMPROGRAMS\Unity Multiversion Launcher"
  CreateShortcut "$SMPROGRAMS\Unity Multiversion Launcher\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortcut "$SMPROGRAMS\Unity Multiversion Launcher\Unity Multiversion Launcher.lnk" "$INSTDIR\uml.exe" "" "$INSTDIR\uml.exe" 0

  CreateShortcut "$DESKTOP\Unity Multiversion Launcher.lnk" "$INSTDIR\uml.exe" "" "$INSTDIR\uml.exe" 0
  
SectionEnd ; end the section

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UnityMultiversionLauncher"
  DeleteRegKey HKLM SOFTWARE\UnityMultiversionLauncher

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*
  ; Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Unity Multiversion Launcher\*.*"
  Delete "$DESKTOP\Unity Multiversion Launcher.lnk"

  ; Remove directories used
  RMDir "$SMPROGRAMS\Unity Multiversion Launcher"
  RMDir "$INSTDIR"

SectionEnd
