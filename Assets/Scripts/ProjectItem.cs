﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProjectItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    private const float LONG_PRESSED_TIME = 1f;

    [SerializeField] private Text _name;
    [SerializeField] private Text _description;

    private string _versionEngine;

    private string _projectPath;

    private bool _isPressed;
    private float _pressedTime;

    void Start () {
		
	}
	
	void Update () {
	    if (_isPressed)
	    {
	        _pressedTime += Time.deltaTime;
	        if (_pressedTime > LONG_PRESSED_TIME)
	        {
                WindowsManager.Instance.ShowUpgradeProjectWindow(_projectPath, _versionEngine);
	            _isPressed = false;
	            _pressedTime = 0;
	        }
	    }
	}

    public void Init(string path)
    {
        _projectPath = path.Substring(0, path.Length-1);
        var pathSplit = path.Split('/');
        var name = pathSplit[pathSplit.Length - 1].Substring(0, pathSplit[pathSplit.Length - 1].Length-1);
        _name.text = name;
        string description = "Path: ";
        string _projectPathCrop = pathSplit[0];
        for (int i = 1; i <= pathSplit.Length - 2; i++)
        {
            _projectPathCrop = String.Concat(_projectPathCrop,Path.DirectorySeparatorChar + pathSplit[i]);
        }

        description += _projectPathCrop;

        description += " | " + "Unity version: " + GetProjectVersion(_projectPathCrop + Path.DirectorySeparatorChar + name);

        _description.text = description;

        _isPressed = false;
        _pressedTime = 0;
    }

    private string GetProjectVersion(string path)
    {
        _versionEngine = "";
        string projectSettingsPath = path + Path.DirectorySeparatorChar + "ProjectSettings";
        
        if (Directory.Exists(projectSettingsPath))
        {            
            var versionPath = projectSettingsPath + Path.DirectorySeparatorChar + "ProjectVersion.txt";
            if (File.Exists(versionPath) == true)
            {
                var data = File.ReadAllLines(versionPath);

                if (data != null && data.Length > 0)
                {
                    var dataFirst = data[0];
                    if (dataFirst.Contains("m_EditorVersion"))
                    {
                        var str = dataFirst.Split(new string[] { "m_EditorVersion: " }, StringSplitOptions.None);
                        if (str != null && str.Length > 0)
                        {
                            _versionEngine = str[1].Trim();
                        }
                    }
                }
            }
        }
        return _versionEngine;
    }
    public void OnMouseOver()
    {

    }

    public void OnMouseExit()
    {

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _isPressed = true;
        _pressedTime = 0;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _isPressed = false;
        _pressedTime = 0;
    }

    public void ProjectClick()
    {
        string pathExe = EngineManager.Instance.GetPath(_versionEngine);
        if (pathExe != "")
        {
            try
            {
                Process myProcess = new Process();
                var cmd = "\"" + pathExe + "\"";
                myProcess.StartInfo.FileName = cmd;
                var pars = " -projectPath " + "\"" + _projectPath + "\"";
                myProcess.StartInfo.Arguments = pars;

                myProcess.Start();

                Application.Quit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        else
        {
            WindowsManager.Instance.ShowEngineNotFoundWindow(_versionEngine);
        }
    }


}
