﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EngineManager : MonoBehaviour
{

    public static EngineManager Instance;
    private const string PREF = "Engine";

    private List<string> _listEngines;
    void Awake()
    {
        Instance = this;
    }
	// Use this for initialization
	void Start () {
	    if (PlayerPrefs.HasKey("listEngines"))
	    {
	        _listEngines = PlayerPrefsX.GetStringArray("listEngines").ToList();
	    }
	    else
	    {
	        _listEngines = new List<string>();
	    }
	}

    // Update is called once per frame
    void Update () {
		
	}

    public void SaveEngine(string version, string path)
    {
        PlayerPrefs.SetString(PREF + version, path);
        if (!_listEngines.Contains(version))
        {
            _listEngines.Add(version);
            SaveListEngines();
        }
    }

    private void SaveListEngines()
    {
        PlayerPrefsX.SetStringArray("listEngines",_listEngines.ToArray());
    }

    public List<string> GetListEngines()
    {
        return _listEngines;
    }

    public string GetPath(string version)
    {
        if (PlayerPrefs.HasKey(PREF + version))
        {
            return PlayerPrefs.GetString(PREF + version);
        }

        return "";
    }

    public void SetDefaultEngine(string version)
    {
        PlayerPrefs.SetString("DefaultEngine",version);
        PlayerPrefs.Save();
    }

    public string GetDefaultEngine()
    {
        if (PlayerPrefs.HasKey("DefaultEngine"))
        {
            return PlayerPrefs.GetString("DefaultEngine");
        }

        return "";
    }

    public void FixedChanges()
    {
        PlayerPrefs.Save();
    }
}
