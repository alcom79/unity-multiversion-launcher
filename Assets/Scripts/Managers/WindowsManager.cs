﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowsManager : MonoBehaviour
{

    public static WindowsManager Instance;

    [SerializeField] private MainWindow _mainWindow;
    [SerializeField] private SettingsWindow _settingsWindow;
    [SerializeField] private ScanUnityEnginesWindow _scanUnityEnginesWindow;
    [SerializeField] private EngineNotFoundWindow _engineNotFoundWindow;
    [SerializeField] private OpenProjectWindow _openProjectWindow;
    [SerializeField] private NewProjectWindow _newProjectWindow;
    [SerializeField] private UpgradeProjectWindow _upgradeProjectWindow;

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () {
		ShowMainWindow();
        HideSettingsWindow();
        HideScanUnityEnginesWindow();
        HideEngineNotFoundWindow();
        HideOpenProjectWindow();
        HideNewProjectWindow();
        HideUpgradeProjectWindow();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowMainWindow()
    {
        _mainWindow.Show();
    }

    public void ShowSettingsWindow()
    {
        _settingsWindow.Show();
    }
    public void HideSettingsWindow()
    {
        _settingsWindow.Hide();
    }

    public void ShowScanUnityEnginesWindow()
    {
        _scanUnityEnginesWindow.Show();
    }

    public void HideScanUnityEnginesWindow()
    {
        _scanUnityEnginesWindow.Hide();
    }

    public void ShowEngineNotFoundWindow(string version)
    {
        _engineNotFoundWindow.Show(version);
    }

    public void HideEngineNotFoundWindow()
    {
        _engineNotFoundWindow.Hide();
    }

    public void ShowOpenProjectWindow()
    {
        _openProjectWindow.Show(true);
    }

    public void HideOpenProjectWindow()
    {
        _openProjectWindow.Hide();
    }

    public void ShowNewProjectWindow()
    {
        _newProjectWindow.Show(true);
    }

    public void HideNewProjectWindow()
    {
        _newProjectWindow.Hide();
    }

    public void ShowUpgradeProjectWindow(string path, string versionEngine)
    {
        _upgradeProjectWindow.Show(path, versionEngine);
    }

    public void HideUpgradeProjectWindow()
    {
        _upgradeProjectWindow.Hide();
    }
}
