﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SFB;
using UnityEngine;
using UnityEngine.UI;

public class ScanUnityEnginesWindow : MonoBehaviour
{

    [SerializeField] private InputField _pathInputField;

    [SerializeField] private Text _logsInputField;

    [SerializeField] private ScrollRect _logsScrollRect;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show()
    {
        this.gameObject.SetActive(true);
        _logsInputField.text = "";
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void SelectFolder()
    {
        var path = StandaloneFileBrowser.OpenFolderPanel("Select root Directory", "", false);
        if (path.Length > 0)
        {
            _pathInputField.text = path[0];
        }
    }

    public void RunClick()
    {
        string root = _pathInputField.text;
        if (Directory.Exists(root) == true)
        {
            bool isFind = false;
            var directories = Directory.GetDirectories(root);
            for (int i = 0, length = directories.Length; i < length; i++)
            {
                string unityExe = directories[i] + Path.DirectorySeparatorChar + "Editor" + Path.DirectorySeparatorChar + "Unity.exe";
                if (File.Exists(unityExe) == true)
                {
                    string uninstallExe = directories[i] + Path.DirectorySeparatorChar + "Editor" + Path.DirectorySeparatorChar + "Uninstall.exe";
                    if (File.Exists(uninstallExe) == true)
                    {
                        FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(uninstallExe);
                        string unityVersion = versionInfo.ProductName.Replace("Unity ", "").Replace(" (64-bit)", "");
                        AddLog("Find Unity "+unityVersion+" on "+unityExe);
                        EngineManager.Instance.SaveEngine(unityVersion, unityExe);
                        isFind = true;
                    }
                }
            }

            if (isFind)
            {
                EngineManager.Instance.FixedChanges();
            }
            else
            {
                AddLog("Engines not found");
            }
        }
        else
        {
            AddLog("Root folder not found");
        }
    }

    private void AddLog(string text)
    {
        if (_logsInputField.text != "") _logsInputField.text += "\n";
        _logsInputField.text += text;
        StartCoroutine(ScrollToBottom());
    }

    IEnumerator ScrollToBottom()
    {
        yield return new WaitForEndOfFrame();
        _logsScrollRect.verticalNormalizedPosition = 0;
    }
}
