﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using UnityEngine;

public class MainWindow : MonoBehaviour
{

    [SerializeField]
    private Transform _listProjectContainer;

    [SerializeField] private GameObject _projectItemPrefab;
	// Use this for initialization
	void Start () {
		ShowProjects();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    private void ShowProjects()
    {
        _listProjectContainer.RemoveChildren();
        RegistryKey rk =
            Registry.CurrentUser.OpenSubKey("Software\\Unity Technologies\\Unity Editor 5.x");

        string[] valueNames = rk.GetValueNames();
        byte[] value;
        string valueAsString;
        GameObject go;
        foreach (string s in valueNames)
        {
            if (s.StartsWith("RecentlyUsedProjectPaths-"))
            {
                value = (byte[]) rk.GetValue(s);
                valueAsString = Encoding.UTF8.GetString(value);

                go = Instantiate(_projectItemPrefab, _listProjectContainer.transform, false) as GameObject;
                go.transform.localScale = Vector3.one;
                go.GetComponent<ProjectItem>().Init(valueAsString.ToString());
            }
        }
    }
}
