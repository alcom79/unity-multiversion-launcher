﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class EngineNotFoundWindow : MonoBehaviour
{

    [SerializeField] private Text _versionText;

    private string _version;
    void Start()
    {

    }

    void Update()
    {

    }

    public void Show(string version)
    {
        _version = version;
        _versionText.text = "Version: " + _version;
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void EngineFindClick()
    {
        WindowsManager.Instance.ShowScanUnityEnginesWindow();
        Hide();
    }

    public void EngineDownloadClick()
    {
        Application.OpenURL(GetDownloadURL());
        Hide();
    }

    private string GetDownloadURL()
    {
        string url = "";
        if (_version.Contains("f"))
        {
            _version = Regex.Replace(_version, @"f.", "", RegexOptions.IgnoreCase);
            url = "https://unity3d.com/unity/whats-new/unity-" + _version;
        }
        if (_version.Contains("p"))
        {
            url = "https://unity3d.com/unity/qa/patch-releases/" + _version;
        }
        if (_version.Contains("b"))
        {
            url = "https://unity3d.com/unity/beta/unity" + _version;
        }
        return url;
    }
}
