﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SFB;
using UnityEngine;
using UnityEngine.UI;

public class OpenProjectWindow : MonoBehaviour {

    [SerializeField] private InputField _pathInputField;

    [SerializeField] private Text _logsInputField;

    [SerializeField] private ScrollRect _logsScrollRect;

    private string _versionEngine;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Show(bool showDialog)
    {
        this.gameObject.SetActive(true);
        _logsInputField.text = "";
        _versionEngine = "";
        _pathInputField.text = "";
        if (showDialog)
        {
            SelectFolder();
        }
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void SelectFolder()
    {
        var path = StandaloneFileBrowser.OpenFolderPanel("Select Project Directory", "", false);
        if (path.Length > 0)
        {
            _pathInputField.text = path[0];
            _versionEngine = DetectVersion(path[0]);
            if (_versionEngine == "")
            {
                AddLog("Project not found");
            }
            else
            {
                AddLog("Found project. Version: "+_versionEngine);
            }
        }
        else
        {
            Hide();
        }
    }

    public void RunClick()
    {
        string pathExe = EngineManager.Instance.GetPath(_versionEngine);
        if (pathExe != "")
        {
            try
            {
                Process myProcess = new Process();
                var cmd = "\"" + pathExe + "\"";
                myProcess.StartInfo.FileName = cmd;
                var pars = " -projectPath " + "\"" + _pathInputField.text + "\"";
                myProcess.StartInfo.Arguments = pars;

                myProcess.Start();

                Hide();
                Application.Quit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        else
        {
            Hide();
            WindowsManager.Instance.ShowEngineNotFoundWindow(_versionEngine);
        }
    }

    private void AddLog(string text)
    {
        if (_logsInputField.text != "") _logsInputField.text += "\n";
        _logsInputField.text += text;
        StartCoroutine(ScrollToBottom());
    }

    IEnumerator ScrollToBottom()
    {
        yield return new WaitForEndOfFrame();
        _logsScrollRect.verticalNormalizedPosition = 0;
    }

    private string DetectVersion(string path)
    {
        var versionEngine = "";
        string projectSettingsPath = path + Path.DirectorySeparatorChar + "ProjectSettings";

        if (Directory.Exists(projectSettingsPath))
        {
            var versionPath = projectSettingsPath + Path.DirectorySeparatorChar + "ProjectVersion.txt";
            if (File.Exists(versionPath) == true)
            {
                var data = File.ReadAllLines(versionPath);

                if (data != null && data.Length > 0)
                {
                    var dataFirst = data[0];
                    if (dataFirst.Contains("m_EditorVersion"))
                    {
                        var str = dataFirst.Split(new string[] { "m_EditorVersion: " }, StringSplitOptions.None);
                        if (str != null && str.Length > 0)
                        {
                            versionEngine = str[1].Trim();
                        }
                    }
                }
            }
        }
        return versionEngine;

    }
}
