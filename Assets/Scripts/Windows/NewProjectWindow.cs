﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SFB;
using UnityEngine;
using UnityEngine.UI;

public class NewProjectWindow : MonoBehaviour {

    [SerializeField] private InputField _pathInputField;
    [SerializeField] private Dropdown _enginesDropdown;

    void Start () {

	}
	
	void Update () {
		
	}
    public void Show(bool showDialog)
    {
        this.gameObject.SetActive(true);
        _pathInputField.text = "";
        var listEngines = EngineManager.Instance.GetListEngines();
        var defaultEngine = EngineManager.Instance.GetDefaultEngine();
        _enginesDropdown.ClearOptions();
        List<Dropdown.OptionData> listOptionData = new List<Dropdown.OptionData>();
        int position = 0;
        for(int i=0; i<listEngines.Count; i++)
        {
            listOptionData.Add(new Dropdown.OptionData(listEngines[i]));
            if (listEngines[i] == defaultEngine) position = i;
        }
        _enginesDropdown.AddOptions(listOptionData);
        _enginesDropdown.value = position;
        if (showDialog)
        {
            SelectFolder();
        }
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void SelectFolder()
    {
        var path = StandaloneFileBrowser.OpenFolderPanel("Select Project Directory", "", false);
        if (path.Length > 0)
        {
            _pathInputField.text = path[0];
        }
        else
        {
            Hide();
        }
    }

    public void RunClick()
    {
        string versionEngine = _enginesDropdown.captionText.text;
        string pathExe = EngineManager.Instance.GetPath(versionEngine);
        if (pathExe != "")
        {
            EngineManager.Instance.SetDefaultEngine(versionEngine);
            try
            {
                Process myProcess = new Process();
                var cmd = "\"" + pathExe + "\"";
                myProcess.StartInfo.FileName = cmd;
                var pars = " -createProject " + "\"" + _pathInputField.text + "\"";
                myProcess.StartInfo.Arguments = pars;

                myProcess.Start();

                Hide();
                Application.Quit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        else
        {
            Hide();
            WindowsManager.Instance.ShowEngineNotFoundWindow(versionEngine);
        }
    }
}
