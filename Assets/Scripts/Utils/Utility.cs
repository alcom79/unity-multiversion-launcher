using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Utility
{
    public static void RemoveChildren(this Transform transform)
    {
        var child = transform.Cast<Transform>().ToArray();
        foreach (var transform1 in child)
        {
            UnityEngine.Object.Destroy(transform1.gameObject);
        }
    }

    public static void RemoveChildren(this Transform transform, params Transform[] except)
    {
        var child = transform.Cast<Transform>().Except(except).ToArray();
        foreach (var transform1 in child)
        {
            UnityEngine.Object.Destroy(transform1.gameObject);
        }
    }

    public static T Random<T>(this IEnumerable<T> collection)
    {
        return collection.ElementAt(UnityEngine.Random.Range(0, collection.Count()));
    }

    public static IEnumerable<T> Random<T>(this IEnumerable<T> collection, int amount)
    {
        var list = collection.ToList();
        if (list.Count < amount)
        {
            throw new ArgumentException("Less elements then needed");
        }
        for (int i = 0; i < amount; i++)
        {
            var element = list.Random();
            list.Remove(element);
            yield return element;
        }
    }
}